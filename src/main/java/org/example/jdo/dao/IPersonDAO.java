package org.example.jdo.dao;

import org.example.jdo.entities.Person;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IPersonDAO extends IDAO<Person, Long> {

    public Person findByName( String name );

}
