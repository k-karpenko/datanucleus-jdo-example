package org.example.jdo.dao;

import org.datanucleus.store.query.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 5:34 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDAO<T, V> implements IDAO<T, V> {

    @Autowired(required = true)
    private PersistenceManagerFactory pmf;

    private final ThreadLocal<PersistenceManager> pm = new ThreadLocal<PersistenceManager>();

    private final Class<T> targetClass;

    protected AbstractDAO(Class<T> targetClass) {
        if (targetClass == null) {
            throw new IllegalArgumentException("<Null>");
        }

        this.targetClass = targetClass;
    }

    protected synchronized PersistenceManager getPM() {
        PersistenceManager pm = this.pm.get();
        if ( pm == null ) {
            this.pm.set( pm = this.pmf.getPersistenceManager() );
        }

        return pm;
    }

    protected <D> Collection<D> processQueryResults( Query query, Object... params  ) {
        Object result = query.execute(params);
        return (Collection<D>) result;
    }

    protected <D> D processQueryResult( Query query, Object... params  ) {
        Object result = query.executeWithArray(params);
        if ( result instanceof QueryResult ) {
            return (D) ((QueryResult) result).iterator().next();
        } else {
            return (D) result;
        }
    }

    @Override
    public Collection<T> findAll() {
        return (Collection<T>) getPM().newQuery(this.targetClass).execute();
    }

    @Override
    public T findOne(V keyValue) {
        return getPM().getObjectById( this.targetClass, keyValue );
    }

    @Override
    public T save(T object) throws DAOException {
        try {
            return getPM().makePersistent(object);
        } catch ( Throwable e ) {
            throw new DAOException( e.getMessage(), e );
        }
    }

    @Override
    public void delete(Long objectId) throws DAOException {
        T object = getPM().getObjectById( this.targetClass, objectId);
        if ( object == null ) {
            throw new DAOException("Requested object not exists");
        }

        getPM().deletePersistent(object);
    }

    @Override
    public void delete(T object) throws DAOException {
        try {
            getPM().deletePersistent(object);
        } catch ( Throwable e ) {
            throw new DAOException( e.getMessage(), e );
        }
    }
}
