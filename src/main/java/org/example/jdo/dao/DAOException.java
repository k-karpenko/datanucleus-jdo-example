package org.example.jdo.dao;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/6/12
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class DAOException extends Exception  {

    public DAOException() {
        this(null);
    }

    public DAOException(String message) {
        this(message, null);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
