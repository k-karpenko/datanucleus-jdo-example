package org.example.jdo.dao;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IDAO<T, V> {

    public Collection<T> findAll();

    public T findOne( V keyValue );

    public T save( T object ) throws DAOException;

    public void delete( T object ) throws DAOException;

    public void delete( Long objectId ) throws DAOException;

}
