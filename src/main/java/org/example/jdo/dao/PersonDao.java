package org.example.jdo.dao;

import org.example.jdo.entities.Person;
import org.springframework.transaction.annotation.Transactional;

import javax.jdo.Query;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 5:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonDao extends AbstractDAO<Person, Long> implements IPersonDAO {

    public PersonDao() {
        super(Person.class);
    }

    @Override
    @Transactional
    public Person findByName(String name) {
        Query query = getPM().newQuery(Person.class);
        query.setFilter("name.equals(value)");
        query.declareParameters("java.lang.String value");

        return this.processQueryResult(query, name);
    }
}
