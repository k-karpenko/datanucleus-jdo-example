package org.example.jdo.entities;

import javax.jdo.annotations.*;
import java.util.ArrayList;
import java.util.Collection;

@PersistenceCapable
public class Person {

    @PrimaryKey
    @Persistent( valueStrategy = IdGeneratorStrategy.IDENTITY )
    private Long id;

    @Persistent
    private String name;

    @Persistent
    private Person parent;

    @Persistent( mappedBy = "parent" )
    @Element(types=Person.class)
    private Collection<Person> dependent = new ArrayList<Person>();

    public Person getParent() {
        return parent;
    }

    public void setParent(Person parent) {
        this.parent = parent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addDependent( Person person ) {
        this.dependent.add( person );
    }

    public Collection<Person> getDependent() {
        return dependent;
    }

    public void setDependent(Collection<Person> dependent) {
        this.dependent = dependent;
    }
}
