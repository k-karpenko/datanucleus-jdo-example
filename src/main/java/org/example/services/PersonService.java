package org.example.services;

import org.example.jdo.dao.DAOException;
import org.example.jdo.entities.Person;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 6:19 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PersonService {

    public boolean checkExists( Long id );

    public Collection<Person> findPersons();

    public void createDependency( Long sourceId, Long targetId );

    public Collection<Person> requestDependencies(Long sourceId);

    public Person requestPerson( Long personId );

    public Person findPerson( String name );

    public Long createPerson( String name ) throws DAOException;

    public void deletePerson( Long id ) throws DAOException;

}
