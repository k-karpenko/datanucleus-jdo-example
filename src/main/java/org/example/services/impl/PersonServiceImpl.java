package org.example.services.impl;

import org.example.jdo.dao.DAOException;
import org.example.jdo.dao.PersonDao;
import org.example.jdo.entities.Person;
import org.example.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 5:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class PersonServiceImpl implements PersonService {
    @Autowired( required = true )
    private PersonDao dao;

    public Long createPerson(String name) throws DAOException {
        Person person = new Person();
        person.setName(name);
        return dao.save(person).getId();
    }

    @Override
    public Person findPerson(String name) {
        return dao.findByName(name);
    }

    @Override
    public boolean checkExists(Long id) {
        return dao.findOne(id) != null;
    }

    @Override
    public Collection<Person> findPersons() {
        return dao.findAll();
    }

    @Override
    public void createDependency(Long sourceId, Long targetId) {
        Person source = this.dao.findOne(sourceId);
        if ( source == null ) {
            throw new IllegalArgumentException("Source not found");
        }

        Person target = this.dao.findOne( targetId );
        if ( target == null ) {
            throw new IllegalArgumentException("Target not found");
        }

        target.addDependent( source );

        try {
            this.dao.save(target);
        } catch ( DAOException e ) {
            throw new IllegalArgumentException("Dependency create failed");
        }
    }

    @Override
    public Collection<Person> requestDependencies(Long sourceId) {
        return this.dao.findOne(sourceId).getDependent();
    }

    @Override
    public Person requestPerson(Long personId) {
        return this.dao.findOne(personId);
    }

    @Override
    public void deletePerson(Long id) throws DAOException {
        dao.delete(id);
    }
}
