import junit.framework.Assert;
import org.example.jdo.entities.Person;
import org.example.services.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 12/5/12
 * Time: 5:18 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/app-context.xml")
public class TestClass {

    private static final Logger logger = Logger.getLogger(TestClass.class.getCanonicalName());

    @Autowired( required = true )
    PersonService service;

    @Test
    public void testMain() throws Exception {
        Long id = service.createPerson("Person #1");
        Assert.assertNotNull(id);
        logger.info("Saved successfully");

        Assert.assertTrue( service.checkExists(id) );

        logger.info("Exists check completed");

        Assert.assertNotNull( service.findPerson("Person #1") );

        logger.info("Find person tested");

        Long newPersonId = service.createPerson("Person #2");
        Person newPerson = service.requestPerson(newPersonId);
        Assert.assertNotNull( "New person request failed", newPerson );

        service.createDependency( id, newPersonId );

        Person person = service.requestPerson(id);
        Assert.assertNotNull("Old person request failed", person);
        Assert.assertFalse("Dependency not established", person.getDependent().isEmpty() );
        Assert.assertTrue("Dependent entity not contents in person dependencies",
                person.getDependent().contains(newPerson) );

        service.deletePerson(id);
    }
}
